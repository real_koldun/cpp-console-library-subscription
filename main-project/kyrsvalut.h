#pragma once
#ifndef BOOK_SUBSCRIPTION_H
#define BOOK_SUBSCRIPTION_H

#include "constants.h"

struct kyrs {
	float kyrs;
};

struct adress {
	char type[MAX_STRING_SIZE];
	char name[MAX_STRING_SIZE];
	int number;
};
struct bank {
	char bankname[MAX_STRING_SIZE];
	kyrs kyrsbuy;
	kyrs kyrssell;
	adress adress;
};

#endif