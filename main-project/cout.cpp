#include <iostream>
#include "constants.h"
#include "file_reader.h"
#include "kyrsvalut.h"
#include "filter.h"
#include "sort.h"
#include "cout.h"
using namespace std;

void coutbanks(bank* banks[], int& size) {
    for (int i = 0; i < size; i++)
    {
        cout << "Bank #" << i + 1 << ": ";
        cout << banks[i]->bankname << endl;
        cout << "Bank buys $: ";
        cout << banks[i]->kyrsbuy.kyrs << endl;
        cout << "Bank sells $: ";
        cout << banks[i]->kyrssell.kyrs << endl;
        cout << "Adress: ";
        cout << banks[i]->adress.name << ' ';
        cout << banks[i]->adress.type << ' ';
        cout << banks[i]->adress.number << endl << endl << endl;
    }
}