#ifndef FILE_READER_H
#define FILE_READER_H

#include "kyrsvalut.h"

void read(const char* file_name, bank* array[], int& size);

#endif
