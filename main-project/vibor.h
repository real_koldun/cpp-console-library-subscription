#pragma once
#include <iostream>
#include "constants.h"
#include "file_reader.h"
#include "kyrsvalut.h"
#include "filter.h"
#include "sort.h"
#include "cout.h"

void filtration(bank* array[], int& size);
void sortirovochka(bank* array[], int& size);