#include <iostream>
#include "constants.h"
#include "file_reader.h"
#include "kyrsvalut.h"
#include "filter.h"
#include "sort.h"
#include "cout.h"
#include "vibor.h"

using namespace std;

int main() {
    cout << "Labaratory work #8. GIT" << endl;
    cout << "Variant #4. Ryrs Valut" << endl;
    cout << "Author: Anton Kitaev" << endl;
    bank* banks[MAX_FILE_ROWS_COUNT];
    int size;
        read("data.txt", banks, size);
        cout << "***** Currency today *****" << endl << endl;
        cout << "Pusirek(vozrastanie): " << endl << endl;
        coutbanks(banks, size);
        filtration(banks, size);
        sortirovochka(banks, size);
    return 0;
}

