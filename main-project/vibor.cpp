#include <iostream>
#include "constants.h"
#include "file_reader.h"
#include "kyrsvalut.h"
#include "filter.h"
#include "sort.h"
#include "cout.h"

using namespace std;

void filtration(bank* array[], int& size) {
    int a;
    cout << endl;
    cout << "Data filtration: " << endl;
    cout << "1. Only Belarusbank " << endl;
    cout << "2. Only $ <2.5 " << endl;
    cout << "Choose...";
    cin >> a;
    cout << endl;
    if (a == 1)
        belarusbank(array, size);
    if (a == 2)
        kyrssort(array, size);
}

void sortirovochka(bank* array[], int& size) {
    int a;
    cout << endl;
    cout << "Sort: " << endl;
    cout << "1. Bubble " << endl;
    cout << "2. Merge " << endl;
    cout << "Choose...";
    cin >> a;
    if (a == 1) {
        cout << "1. Lower to higher " << endl;
        cout << "2. Higher to lower" << endl;
        cin >> a;
        cout << endl;
        if (a == 1)
            sortbubblevoz(array, size);
        if(a==2)
            sortbubbleub(array, size);
        a = 0;
    }
    if (a == 2) {
        cout << "1. Lower to higher " << endl;
        cout << "2. Higher to lower" << endl;
        cin >> a;
        if (a == 1)
            sortbubblevoz(array, size);
        if (a == 2)
            sortbubbleub(array, size);
    }
    coutbanks(array, size);
}