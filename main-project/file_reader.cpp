#include "file_reader.h"
#include "constants.h"

#include <fstream>
#include <cstring>
#include <string>
#include <iostream>
using namespace std;
kyrs convert(char* str)
{
    kyrs result;
    char* context = NULL;
    char* str_number = strtok_s(str, " ", &context);
    result.kyrs = std::stof (str_number);
    return result;
}

void read(const char* file_name, bank* array[], int& size)
{
    std::ifstream file(file_name);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
            while (!file.eof())
            { 
                bank* kyrsvalut = new bank;
                file >> kyrsvalut->bankname;
                file >> tmp_buffer;
                kyrsvalut->kyrsbuy = convert(tmp_buffer);
                file >> tmp_buffer;
                kyrsvalut->kyrssell = convert(tmp_buffer);
                file >> kyrsvalut->adress.name;
                file >> kyrsvalut->adress.type;
                file >> kyrsvalut->adress.number;
                file.read(tmp_buffer, 1); // ������ ������� ������� �������
                array[size] = kyrsvalut;
                size++;
            }
            file.close();
    }
    else
    {
        throw "������ �������� �����";
    }
}